# multiarch-qemu
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/multiarch-qemu)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/multiarch-qemu)



----------------------------------------
### x64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/multiarch-qemu/x64)



----------------------------------------
#### Description

* Distribution : [Debian GNU/Linux](https://www.debian.org/)
* Architecture : x64
* Appplication : [Qemu](https://github.com/balena-io/qemu/)
    - QEMU with additional QEMU_EXECVE flag that persists emulator after an execve.



----------------------------------------
#### Run

* Nothing



----------------------------------------
#### Usage

* Nothing



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

